#include <iostream>
#include "Polygon.h"

int main()
{
    std::cout << "Hello, World!" << std::endl;
    Polygon poly(5);
    poly.setPointByIndex(Point(4,4), 2);
    poly.getPointReferenceByIndex(3).setX(99); //change x-coordinate of point via reference
    poly.outputPointArray();
    return 0;
}