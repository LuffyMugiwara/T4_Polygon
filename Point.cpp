

#include "Point.h"

Point::Point(size_t X, size_t Y)
:X_m(X),
 Y_m(Y)
{

}

Point::Point()
:X_m(0),
 Y_m(0)
{

}

Point::Point(const Point& point)
:X_m(0),
 Y_m(0)
{
    *this = point;
}

Point::~Point()
{

}

Point& Point::operator=(const Point& point)
{
    if(this != &point)
    {
        X_m = point.X_m;
        Y_m = point.Y_m;
    }
    return *this;
}

size_t Point::getX() const
{
    return X_m;
}

size_t Point::getY() const
{
    return Y_m;
}

const void Point::setX(size_t X)
{
    X_m = X;
}

const void Point::setY(size_t Y)
{
    Y_m = Y;
}


