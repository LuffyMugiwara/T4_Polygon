
#ifndef T4_POLYGON_POLYGON_H
#define T4_POLYGON_POLYGON_H


#include "Point.h"

class Polygon {

    //members
    public:


    private:
        size_t pointCount_m;
        Point* pointArray_m;


    //methods
    public:
        Polygon(const size_t pointCount);
        virtual ~Polygon();
        Polygon(const Polygon& polygon);
        Polygon& operator=(const Polygon& polygon);

        Point getPointByIndex(const size_t index) const;
        Point& getPointReferenceByIndex(const size_t index);
        const void setPointByIndex(const Point point, const size_t index);

        size_t getPointCount() const;
        const void outputPointArray() const;

    private:

};


#endif //T4_POLYGON_POLYGON_H
