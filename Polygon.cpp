
#include <cstring>
#include "Polygon.h"



Polygon::Polygon(const size_t pointCount)
:pointCount_m(pointCount),
 pointArray_m(0)
{

    //Point * pointArray = new Point[100];
    //delete [] pointArray; // when you're done


    pointArray_m = new Point[pointCount_m];

    for (size_t i = 0; i < pointCount_m; i++)
        pointArray_m[i] = Point(1,1);


}

Polygon::Polygon(const Polygon& polygon)
:pointCount_m(0),
 pointArray_m(0)
{
    *this = polygon;
}

Polygon::~Polygon()
{
    std::cout << "Free Memory!" << std::endl;
    delete[] pointArray_m;
}


Polygon& Polygon::operator=(const Polygon& polygon)
{
    if(this != &polygon)
    {
        pointCount_m = polygon.pointCount_m;
        delete[] pointArray_m;
        pointArray_m = new Point[pointCount_m];
        memcpy(pointArray_m, polygon.pointArray_m, pointCount_m* sizeof(Point) );
    }
    return *this;
}

const void Polygon::outputPointArray() const
{
    for(size_t i = 0; i < pointCount_m; i++)
        std::cout << "X: " << pointArray_m[i].getX() << " Y: " << pointArray_m[i].getY() << std::endl;
}


Point Polygon::getPointByIndex(const size_t index) const
{
    return pointArray_m[index];
}

Point& Polygon::getPointReferenceByIndex(const size_t index)
{
    return pointArray_m[index];
}

const void Polygon::setPointByIndex(const Point point, const size_t index)
{
    pointArray_m[index] = point;
}


size_t Polygon::getPointCount() const
{
    return pointCount_m;
}










