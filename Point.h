
#ifndef T4_POLYGON_POINT_H
#define T4_POLYGON_POINT_H


#include <iostream>

class Point {

    //members

    public:



private:
    size_t X_m;
    size_t Y_m;

    //methods

    public:
        Point();
        Point(const size_t X, const size_t Y);
        virtual ~Point();
        Point(const Point& point );

        Point& operator=(const Point& point);

        size_t getX() const;
        size_t getY() const;
        const void setX(size_t X);
        const void setY(size_t Y);

    private:


};


#endif //T4_POLYGON_POINT_H
